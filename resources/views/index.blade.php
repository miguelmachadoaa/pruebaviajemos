<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Prueba Viajemos.com</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{secure_url('assets/css/fontawesome/css/all.css')}}">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <style> 
    #map {
        height: 30em;
        }
     
        html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        }
    </style> 
    </head>
    <body >
 

<main role="main">

  <section class="jumbotron text-center" style="background: url('{{secure_url('assets/images/clima.jpg')}}'); background-size: cover;">
    <div class="container">
      <h1>Buscar Clima <i class="fa fa-umbrella"></i></h1>

      <div>
      <div class="form-group">
        <label for="exampleInputEmail1">Puede buscar por ciudad</label>
        <input type="text" class="form-control" id="buscar" name="buscar" aria-describedby="emailHelp">
      </div>
      <button type="button" class="btn btn-primary btnbuscar">Buscar </button>
      <p><a style="background-color: rgba(255,255,255,0.7);
color: #333;" class="btn btn-link"  href="{{secure_url('historico')}}">Ver Historico de Busquedas</a></p>
    </div>
      
    </div>
  </section>

    <div class="container contenido" >

        <div class="row">

            <div class="col-sm-6">
                <h3 class="ciudad">Ciudad</h3>
                <h1 class="condicion">Pais</h1>
                <h1 class="temperatura">30</h1>

                <p class="viento_frio"></p>
                <p class="viento_velocidad"></p>
                <p class="humedad"></p>
                <p class="visibilidad"></p>
                <p class="presion"></p>

                <br>

                <h3>Pronostico</h3>

                <table class="table ">
                    <tr>
                        <th>Dia</th>
                        <th>Bajo</th>
                        <th>Alto</th>
                        <th>Condicion</th>

                        <tr>
                            <td class="dia1dia"></td>
                            <td class="dia1bajo"></td>
                            <td class="dia1alto"></td>
                            <td class="dia1condicion"></td>

                            <tr>
                            <td class="dia2dia"></td>
                            <td class="dia2bajo"></td>
                            <td class="dia2alto"></td>
                            <td class="dia2condicion"></td>
                        </tr>

                        <tr>
                            <td class="dia3dia"></td>
                            <td class="dia3bajo"></td>
                            <td class="dia3alto"></td>
                            <td class="dia3condicion"></td>
                        </tr>

                        <tr>
                            <td class="dia4dia"></td>
                            <td class="dia4bajo"></td>
                            <td class="dia4alto"></td>
                            <td class="dia4condicion"></td>
                        </tr>


                        <tr>
                            <td class="dia5dia"></td>
                            <td class="dia5bajo"></td>
                            <td class="dia5alto"></td>
                            <td class="dia5condicion"></td>
                        </tr>

                        <tr>
                            <td class="dia6dia"></td>
                            <td class="dia6bajo"></td>
                            <td class="dia6alto"></td>
                            <td class="dia6condicion"></td>
                        </tr>


                        <tr>
                            <td class="dia7dia"></td>
                            <td class="dia7bajo"></td>
                            <td class="dia7alto"></td>
                            <td class="dia7condicion"></td>
                        </tr>

                        </tr>
                    </tr>


                </table>

                
            </div>

           




        <div class="col-sm-6">
            <div id ="map"> </div> 
        </div>
            


        </div>

            
    </div>

</main>
<input type="hidden" id="base" name="base" value="{{secure_url('/')}}">



    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOI2Lfi8Pt_0f4ZdEpfWHB9hZAXTaBWhw&callback=initMap">
    </script>

    <script  type="text/javascript" charset="utf-8" async defer> 


        $(document).ready(function(){

            $('.contenido').fadeOut();
        });

        var lat=0;
        var lng=0;


                    var map;
                function initMap() {

                    map = new google.maps.Map(document.getElementById('map'), {
                      center: {lat: 4, lng: -74},
                      zoom: 5
                    });

                    var marker = new google.maps.Marker({

                      position: {lat: 4, lng: -74},
                      map: map,
                        title: 'Ubicacion Actual '
                    });

                }


            $('.btnbuscar').on('click', document, function(){

                base=$('#base').val();

                termino=$('#buscar').val();

                $.ajax({
                    url: base+'/getclima/'+termino,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        let iconos=['<i class="fa fa-wind"></i>','<i class="fa fa-wind"></i>','<i class="fa fa-wind"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-cloud-rain"></i>','<i class="fa fa-cloud-showers-heavy"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-snowflake"></i>' ];

                        //var content = JSON.parse(data);
                        //alert(data.location.city);

                        $('.ciudad').html(data.location.city+', '+data.location.region+','+data.location.country);
                        $('.condicion').html(iconos[data.current_observation.condition.code]+' '+data.current_observation.condition.text);

                         c=(data.current_observation.condition.temperature-32)*(5/9);


                        $('.temperatura').html(data.current_observation.condition.temperature+' °F /'+c.toFixed(0)+' °C');


                         c=(data.current_observation.wind.chill-32)*(5/9);
                        

                        $('.viento_frio').html('Sensacion Termica: '+data.current_observation.wind.chill+' °F /'+(c.toFixed(0)+' °C'));
                        $('.viento_velocidad').html('Velocidad del Viento: '+data.current_observation.wind.speed);
                        $('.humedad').html('Humedad: '+data.current_observation.atmosphere.humidity);
                        $('.visibilidad').html('Visibilidad: '+data.current_observation.atmosphere.visibility);
                        $('.presion').html('Presion: '+data.current_observation.atmosphere.pressure);





                        map = new google.maps.Map(document.getElementById('map'), {
                          center: {lat: data.location.lat, lng: data.location.long},
                          zoom: 5
                        });

                        var marker = new google.maps.Marker({
                          position: {lat: data.location.lat, lng: data.location.long},
                          map: map,
                            title: data.location.city
                        });


                        $('.dia1dia').html(data.forecasts[0].day);
                         c=(data.forecasts[0].low-32)*(5/9);
                        $('.dia1bajo').html(data.forecasts[0].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[0].high-32)*(5/9);
                        $('.dia1alto').html(data.forecasts[0].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia1condicion').html(iconos[data.forecasts[0].code]+' '+data.forecasts[0].text);

                        $('.dia2dia').html(data.forecasts[1].day);
                        c=(data.forecasts[1].low-32)*(5/9);
                        $('.dia2bajo').html(data.forecasts[1].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[1].high-32)*(5/9);
                        $('.dia2alto').html(data.forecasts[1].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia2condicion').html(iconos[data.forecasts[1].code]+' '+data.forecasts[1].text);


                        $('.dia3dia').html(data.forecasts[2].day);
                        c=(data.forecasts[2].low-32)*(5/9);
                        $('.dia3bajo').html(data.forecasts[2].low+' °F /'+c.toFixed(0)+'°C');
                         c=(data.forecasts[2].high-32)*(5/9);
                        $('.dia3alto').html(data.forecasts[2].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia3condicion').html(iconos[data.forecasts[2].code]+' '+data.forecasts[2].text);


                        $('.dia4dia').html(data.forecasts[3].day);
                        c=(data.forecasts[3].low-32)*(5/9);
                        $('.dia4bajo').html(data.forecasts[3].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[3].high-32)*(5/9);
                        $('.dia4alto').html(data.forecasts[3].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia4condicion').html(iconos[data.forecasts[3].code]+' '+data.forecasts[3].text);

                        $('.dia5dia').html(data.forecasts[4].day);
                        c=(data.forecasts[4].low-32)*(5/9);
                        $('.dia5bajo').html(data.forecasts[4].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[4].high-32)*(5/9);
                        $('.dia5alto').html(data.forecasts[4].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia5condicion').html(iconos[data.forecasts[4].code]+' '+data.forecasts[4].text);


                        $('.dia6dia').html(data.forecasts[5].day);
                        c=(data.forecasts[5].low-32)*(5/9);
                        $('.dia6bajo').html(data.forecasts[5].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[5].high-32)*(5/9);
                        $('.dia6alto').html(data.forecasts[5].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia6condicion').html(iconos[data.forecasts[5].code]+' '+data.forecasts[5].text);


                        $('.dia7dia').html(data.forecasts[6].day);
                        c=(data.forecasts[6].low-32)*(5/9);
                        $('.dia7bajo').html(data.forecasts[6].low+' °F /'+c.toFixed(0)+'°C');
                        c=(data.forecasts[6].high-32)*(5/9);
                        $('.dia7alto').html(data.forecasts[6].high+' °F /'+c.toFixed(0)+'°C');
                        $('.dia7condicion').html(iconos[data.forecasts[6].code]+' '+data.forecasts[6].text);


                        $('.contenido').fadeIn();

                    }
                });

            });

               


    </script>


       
        
    </body>
</html>
