<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Prueba Viajemos.com</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{secure_url('assets/css/fontawesome/css/all.css')}}">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <style> 
    #map {
        height: 30em;
        }
     
        html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        }
    </style> 
    </head>
    <body >
 

<main role="main">

  <section class="jumbotron text-center" style="background: url('{{secure_url('assets/images/clima.jpg')}}'); background-size: cover;">
    <div class="container">
      <h1>Historico de Busquedas <i class="fa fa-umbrella"></i></h1>
      <p><a style="background-color: rgba(255,255,255,0.7);
color: #333;" class="btn btn-link"  href="{{secure_url('/')}}">Inicio</a></p>
      
      
    </div>
  </section>

    <div class="container " >

       
        <div class="row">
            <div class="col-sm-12">
                <table class="table ">
                    <tr>
                        <th>Termino</th>
                        <th>Respuesta</th>
                        <th>Fecha</th>
                    </tr>

                    @foreach($busquedas as $b)

                    @if(isset(json_decode($b->json)->location->city))

                    <tr>
                        <td>{{$b->termino}}</td>
                        <td>

                           <p>{{json_decode($b->json)->location->city.' ,'.json_decode($b->json)->location->region.', '.json_decode($b->json)->location->country}}</p> 
                           <p><b>Clima:</b> {{json_decode($b->json)->current_observation->condition->text}}</p>
                           <p><b>Temperatura:</b> {{json_decode($b->json)->current_observation->condition->temperature}} °F</p>
                            

                        </td>
                        <td>{{$b->created_at}}</td>
                    </tr>

                    @endif


                    @endforeach
                </table>
            </div>
        </div>
            
    </div>

</main>
<input type="hidden" id="base" name="base" value="{{secure_url('/')}}">



    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOI2Lfi8Pt_0f4ZdEpfWHB9hZAXTaBWhw&callback=initMap">
    </script>

    <script  type="text/javascript" charset="utf-8" async defer> 


        $(document).ready(function(){

            $('.contenido').fadeOut();
        });

        var lat=0;
        var lng=0;


                    var map;
                function initMap() {

                    map = new google.maps.Map(document.getElementById('map'), {
                      center: {lat: 4, lng: -74},
                      zoom: 5
                    });

                    var marker = new google.maps.Marker({

                      position: {lat: 4, lng: -74},
                      map: map,
                        title: 'Ubicacion Actual '
                    });

                }


            $('.btnbuscar').on('click', document, function(){

                base=$('#base').val();

                termino=$('#buscar').val();

                $.ajax({
                    url: base+'/getclima/'+termino,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        let iconos=['<i class="fa fa-wind"></i>','<i class="fa fa-wind"></i>','<i class="fa fa-wind"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-cloud-rain"></i>','<i class="fa fa-cloud-showers-heavy"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-cloud"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-sun"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-bolt"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-snowflake"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-poo-storm"></i>','<i class="fa fa-snowflake"></i>' ];

                        //var content = JSON.parse(data);
                        //alert(data.location.city);

                        $('.ciudad').html(data.location.city+', '+data.location.region+','+data.location.country);
                        $('.condicion').html(iconos[data.current_observation.condition.code]+' '+data.current_observation.condition.text);
                        $('.temperatura').html(data.current_observation.condition.temperature+' °F');





                        $('.viento_frio').html('Sensacion Termica: '+data.current_observation.wind.chill);
                        $('.viento_velocidad').html('Velocidad del Viento: '+data.current_observation.wind.speed);
                        $('.humedad').html('Humedad: '+data.current_observation.atmosphere.humidity);
                        $('.visibilidad').html('Visibilidad: '+data.current_observation.atmosphere.visibility);
                        $('.presion').html('Presion: '+data.current_observation.atmosphere.pressure);





                        map = new google.maps.Map(document.getElementById('map'), {
                          center: {lat: data.location.lat, lng: data.location.long},
                          zoom: 5
                        });

                        var marker = new google.maps.Marker({
                          position: {lat: data.location.lat, lng: data.location.long},
                          map: map,
                            title: data.location.city
                        });


                        $('.dia1dia').html(data.forecasts[0].day);
                        $('.dia1bajo').html(data.forecasts[0].low+' °F');
                        $('.dia1alto').html(data.forecasts[0].high+' °F');
                        $('.dia1condicion').html(iconos[data.forecasts[0].code]+' '+data.forecasts[0].text);

                        $('.dia2dia').html(data.forecasts[1].day);
                        $('.dia2bajo').html(data.forecasts[1].low+' °F');
                        $('.dia2alto').html(data.forecasts[1].high+' °F');
                        $('.dia2condicion').html(iconos[data.forecasts[1].code]+' '+data.forecasts[1].text);


                        $('.dia3dia').html(data.forecasts[2].day);
                        $('.dia3bajo').html(data.forecasts[2].low+' °F');
                        $('.dia3alto').html(data.forecasts[2].high+' °F');
                        $('.dia3condicion').html(iconos[data.forecasts[2].code]+' '+data.forecasts[2].text);


                        $('.dia4dia').html(data.forecasts[3].day);
                        $('.dia4bajo').html(data.forecasts[3].low+' °F');
                        $('.dia4alto').html(data.forecasts[3].high+' °F');
                        $('.dia4condicion').html(iconos[data.forecasts[3].code]+' '+data.forecasts[3].text);

                        $('.dia5dia').html(data.forecasts[4].day);
                        $('.dia5bajo').html(data.forecasts[4].low+' °F');
                        $('.dia5alto').html(data.forecasts[4].high+' °F');
                        $('.dia5condicion').html(iconos[data.forecasts[4].code]+' '+data.forecasts[4].text);


                        $('.dia6dia').html(data.forecasts[5].day);
                        $('.dia6bajo').html(data.forecasts[5].low+' °F');
                        $('.dia6alto').html(data.forecasts[5].high+' °F');
                        $('.dia6condicion').html(iconos[data.forecasts[5].code]+' '+data.forecasts[5].text);


                        $('.dia7dia').html(data.forecasts[6].day);
                        $('.dia7bajo').html(data.forecasts[6].low+' °F');
                        $('.dia7alto').html(data.forecasts[6].high+' °F');
                        $('.dia7condicion').html(iconos[data.forecasts[6].code]+' '+data.forecasts[6].text);


                        $('.contenido').fadeIn();

                    }
                });

            });

               


    </script>


       
        
    </body>
</html>
