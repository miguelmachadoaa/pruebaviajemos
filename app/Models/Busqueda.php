<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Busqueda extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'busquedas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'termino',
        'json',
    ];

     protected $casts = [
        'termino' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'termino' => 'required'
    ];
}
