<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Busqueda;


class InicioController extends Controller
{
    //
    
    public function index()
    {

    	

    	return view('index');
    }


    public function historico()
    {

    	
    	$busquedas=Busqueda::select('busquedas.*')
    	->orderBy('id', 'desc')
    	->limit(24)
    	->get();

    	return view('historico', compact('busquedas'));
    }




   public function buildBaseString($baseURI, $method, $params) {
	    $r = array();
	    ksort($params);
	    foreach($params as $key => $value) {
	        $r[] = "$key=" . rawurlencode($value);
	    }
	    return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
	}

	public function buildAuthorizationHeader($oauth) {
	    $r = 'Authorization: OAuth ';
	    $values = array();
	    foreach($oauth as $key=>$value) {
	        $values[] = "$key=\"" . rawurlencode($value) . "\"";
	    }
	    $r .= implode(', ', $values);
	    return $r;
	}


	public function getclima($termino)
	{


		$url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
		$app_id = 'pFgk3HyV';
		$consumer_key = 'dj0yJmk9WFJZTUNQWDZEa1dYJmQ9WVdrOWNFWm5hek5JZVZZbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTQ3';
		$consumer_secret = '20226be199cafa2ab73b1690fc3f7fbd3a03dd99';

		$query = array(
		    'location' => $termino,
		    'format' => 'json',
		);

		$oauth = array(
		    'oauth_consumer_key' => $consumer_key,
		    'oauth_nonce' => uniqid(mt_rand(1, 1000)),
		    'oauth_signature_method' => 'HMAC-SHA1',
		    'oauth_timestamp' => time(),
		    'oauth_version' => '1.0'
		);

		$base_info = $this->buildBaseString($url, 'GET', array_merge($query, $oauth));

		

		$composite_key = rawurlencode($consumer_secret) . '&';
		$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
		$oauth['oauth_signature'] = $oauth_signature;

		$header = array(
		    $this->buildAuthorizationHeader($oauth),
		    'X-Yahoo-App-Id: ' . $app_id
		);


		$options = array(
		    CURLOPT_HTTPHEADER => $header,
		    CURLOPT_HEADER => false,
		    CURLOPT_URL => $url . '?' . http_build_query($query),
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_SSL_VERIFYPEER => false
		);

		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//echo ($response);
		$return_data = json_decode($response);

		//dd($response);
		//
		

		$data_busqueda = array(
			'termino' => $termino, 
			'json' => $response
		);

		Busqueda::create($data_busqueda);

		
		return $response;

	}
}
